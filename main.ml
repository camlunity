
open Prelude

let fcgi_port = ref 0

let () =
  let args = Arg.align
  [
    "-fcgi", Arg.Set_int fcgi_port, "<port> run as fcgi";
  ]
  in
  Arg.parse args (fail "Unknown argument %s") "Allowed options:"

let fcgi_port = !fcgi_port

let run_cgi () =
  Netcgi_cgi.run 
    ~output_type:(`Direct "")
    (Request.main true)

let run_fcgi () =
  Netcgi_fcgi.run 
    ~sockaddr:(Unix.ADDR_INET (Unix.inet_addr_loopback,fcgi_port))
    ~output_type:(`Direct "")
    (Request.main false)

let main () =
  match fcgi_port with
  | 0 -> run_cgi ()
  | _ -> run_fcgi ()

let () =
  Printexc.record_backtrace true;
  try
    main ()
  with
    e -> 
      print_endline (Printexc.to_string e);
      print_endline (Printexc.get_backtrace ())


