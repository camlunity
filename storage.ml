
open Prelude

type key = [ `I of int | `S of string ]

module type T = sig

type t
val create : string -> t
val close : t -> unit
val get : t -> key -> string option
val add : t -> key -> string -> unit

end

module Fs : T = struct

type t = string
let create path = (try Unix.mkdir path 0o777 with Unix.Unix_error (Unix.EEXIST,_,_) -> () | e -> raise e); path
let close _ = ()
let make = function
  | `I n -> string_of_int n
  | `S s -> Digest.to_hex (Digest.string s)
let get path name = try Std.input_file (Filename.concat path (make name)) >> some with _ -> None
let add path name value = Std.output_file (Filename.concat path (make name)) value

end

