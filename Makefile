
.PHONY: build clean

build:
		ocamlbuild -j 0 main.native

clean:
		ocamlbuild -clean
