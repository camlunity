
open Printf

class en = object
method main_page = "Main page"
method add_comment = "Add"
method not_found = "Not found"
end

class ru = object
inherit en
method main_page = "Главная страница"
method add_comment = "Добавить"
method view_comment = "Просмотреть"
method edit_comment = "Редактировать"
method no_item = sprintf "Нет такого : %s"
end
