
open Printf
open Netcgi
open ExtLib

open Prelude

let encode = Netencoding.Html.encode ~in_enc:`Enc_utf8 ~out_enc:`Enc_utf8 ()

let main is_cgi (cgi:cgi) =
  let env = cgi#environment in
(*   let cgi_arg name = try Some (cgi#argument name)#value with _ -> None in *)
  let outs = cgi#out_channel#output_string in
  let out fmt = ksprintf outs fmt in
  let serve_text () = cgi#set_header ~cache:`No_cache ~content_type:"text/plain" () in
  let serve_html ?(status=`Ok) html =
    match html with
    | `Redirect url -> cgi#set_redirection_header url
    | `Content html ->
      cgi#set_header ~cache:`No_cache ~content_type:"text/html" ();
      XHTML.M.output ~encode ~encoding:"utf-8" outs html
  in
  let prefix s url = if String.starts_with url "/" then sprintf "/%s%s" (String.strip ~chars:"/" s) url else url in
  let render page =
    try
      let html = page#render (cgi#arguments:><name:string;value:string> list) in
      `Content (if is_cgi then XHTML.M.rewrite_hrefs (prefix env#cgi_script_name) html else html)
    with
      Page.Redirect path -> 
        `Redirect (sprintf "%s%s" 
          (cgi#url ~with_script_name:(if is_cgi then `Env else `None) ~with_path_info:`None ~with_query_string:`None ()) 
          path)
  in

  let dump_cgi () =
    out "cwd : %s\n" (Unix.getcwd ());
    out "\ncgi_properties\n";
    List.iter (fun (k,v) -> out "%s = %s\n" k v) env#cgi_properties;
    out "\ninput_header_fields\n";
    List.iter (fun (k,v) -> out "%s = %s\n" k v) env#input_header_fields;
    out "\ncgi_arguments\n";
    List.iter (fun x -> out "%s = %s\n" x#name x#value) cgi#arguments
  in

  let path = if is_cgi then env#cgi_path_info else env#cgi_script_name in
  let path = match path with "" -> "/" | s -> s in

  match catch (Page.resolve (Page.main:>Page.base)) path with
  | Some page -> page >> render >> serve_html
  | None ->
    match path with
    | "/dump" -> serve_text (); dump_cgi ()
    | _ -> new Page.not_found path >> render >> serve_html ~status:`Not_found

let main is_cgi cgi =
  try
    main is_cgi (cgi:>Netcgi.cgi);
    cgi#out_channel#commit_work ();
  with
    e -> 
    cgi#out_channel#rollback_work ();
    cgi#set_header ~cache:`No_cache ~content_type:"text/plain" ~status:`Internal_server_error ();
    cgi#out_channel#output_string (Printexc.to_string e);
    cgi#out_channel#commit_work ()

