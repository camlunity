
open XHTML.M
open ExtLib

open Prelude

let t = pcdata

let document heading extra x =
  html ~a:[a_xmlns `W3_org_1999_xhtml; a_xml_lang "en"]
  (head (title (t heading)) extra)
  (body x)

let doc heading x = document heading [] x

let input_text ~title ?(id="") ?(init="") () =
  form ~a:[a_method `Post] ~action:(uri_of_string "")
   (h1 [t title])
   [
     (p [textarea ~rows:20 ~cols:80 ~a:[a_name "text"] (t init)]);
     (p [
      input ~a:[a_name "id"; a_value id; a_input_type `Hidden] ();
      input ~a:[a_name "submit"; a_value "Submit"; a_input_type `Submit] ()
      ]);
   ]

let ahref s = a ~a:[a_href (uri_of_string s)]

let ul = function
  | [] -> p [t"?"]
  | h::t -> ul h t

let arg x = catch (fun args -> let a = List.find (fun a -> a#name = x) args in a#value)

type location = string

(* **************************** *)

let z = new Lang.ru
module Store = Storage.Fs

let rec resolve self url = 
  if url = self#url then self
  else List.find (fun child -> try ignore (resolve child url); true with Not_found -> false) self#children

class virtual base =
object(self)
method virtual url : location
method virtual name : string
method virtual children : base list
method virtual render : <name:string;value:string> list -> html
end

let header self pages =
  let name x = if Oo.id x = Oo.id self then [b [t x#name]] else [t x#name] in
  div [p (List.map (fun p -> [ahref p#url (name p); space ()]) pages >> List.flatten); hr ()]

let render_markdown s =
  let module SM = Simple_markup in
  let render_pre ~kind s = t s
  and render_link href = ahref href.SM.href_target [t href.SM.href_desc]
  and render_img i = img ~src:(uri_of_string i.SM.img_src) ~alt:i.SM.img_alt () in
  s >> SM.parse_text >> Simple_markup__html.to_html ~render_pre ~render_link ~render_img
 
let get_index store = 
  match Store.get store (`S "index") with
  | None -> []
  | Some s -> Marshal.from_string s 0

let log_exn e fmt = Printf.ksprintf print_endline fmt

class view_comment anc = object(self)
inherit base
method children = []
method url = "/view"
method name = z#view_comment
method render args =
  let store = Store.create "comments" in
  let show_index () =
    ul (List.map 
      (fun x -> li [ahref (Printf.sprintf "%s?id=%u" self#url x) [t (string_of_int x)]]) 
      (List.sort ~cmp:compare (get_index store)))
  in
  let content = match arg "id" args with
  | Some v ->
    begin 
    try
      match Store.get store (`I (int_of_string v)) with
      | Some s -> render_markdown s >> div
      | None -> div [h1 [t (z#no_item v)]; show_index ()]
    with e -> log_exn e "view_comments(%s)" v; show_index ()
    end
  | None -> show_index ()
  in
  doc self#name
  [
    header self [anc];
    content;
  ]

end

exception Redirect of string

class comment ctx = object(self)
inherit base
method children = []
method url = "/comment"
method name = z#add_comment
method render args =
  let text =
  match arg "text" args with
  | Some text -> 
    let store = Store.create "comments" in
    let index = get_index store in
    let id = match List.sort ~cmp:(flip compare) index with
    | [] -> 0
    | h::_ -> h + 1
    in
    (* FIXME atomicity *)
    Store.add store (`S "index") (Marshal.to_string (id::index) []);
    Store.add store (`I id) text;
    raise (Redirect (Printf.sprintf "/view?id=%u" id))
  | None -> div [p [t ""]]
  in
  doc self#name
  [
    header self [ctx];
    text;
    input_text self#name ()
  ]
end

class edit_comment ctx = object(self)
inherit base
method children = []
method url = "/edit_comment"
method name = z#edit_comment
method render args =
  let store = Store.create "comments" in
  let show_index () =
    ul (List.map 
      (fun x -> li [ahref (Printf.sprintf "%s?id=%u" self#url x) [t (string_of_int x)]]) 
      (List.sort ~cmp:compare (get_index store)))
  in
  try
  match arg "id" args, arg "text" args with
  | Some n, Some text ->
    let n = int_of_string n in
    (match Store.get store (`I n) with
    | Some _ -> Store.add store (`I n) text
    | None -> ());
    raise (Redirect (Printf.sprintf "/view?id=%u" n))
  | None, _ -> 
    doc self#name
    [
      header self [ctx];
      show_index ()
    ]
  | Some n, None -> 
    doc self#name
    [
      header self [ctx];
      input_text ~init:(Store.get store (`I (int_of_string n)) >> Option.default "") ~id:n ~title:self#name ()
    ]
  with 
    | Redirect r -> raise (Redirect r)
    | e -> log_exn e "edit_comment"; doc self#name [ header self [ctx]; show_index ()]
end

class main = object(self)
inherit base
method comment = new comment self
method children = ([new comment self; new view_comment self; new edit_comment self]:>base list)
method url = "/"
method name = z#main_page 
method render args =
  doc self#name
  [
    header self [self];
    ul (List.map (fun x -> li [ahref x#url [t x#name]]) self#children);
  ]
end

let main = new main

class not_found url =
object(self)
inherit base
method url = url
method children = []
method name = z#not_found
method render _ =
  doc self#name
  [
    header self [main];
    p [t "Url "; b [t url]; t " not found."];
  ]

end

