open Ocamlbuild_plugin
open Command

module C = Myocamlbuild_config

;;

let () =
  let bracket res destroy k = let x = (try k res with e -> destroy res; raise e) in destroy res; x in
  let get_line r d = bracket r d input_line in

  bracket (open_out "version.ml") close_out (fun out ->
   let revision =
    try
     get_line (Unix.open_process_in "git describe --always") (Unix.close_process_in)
    with
     _ -> (try get_line (open_in "version.id") close_in with _ -> "<unknown>")
   in
   Printf.fprintf out "let id=\"%s\"\n" (String.escaped revision)
  )

;;

dispatch begin function
| After_rules ->

     C.extern ~cma:"extLib" "extlib";
     C.extern "pcre";
     C.extern "netsys";
     C.extern "netstring";
     C.extern "equeue";
     C.extern "netclient";
     C.extern "netcgi2" ~cma:"netcgi";
     C.extern "oUnit";
     C.extern "zip";
     C.extern "json-wheel" ~cma:"jsonwheel";
     C.extern "sqlite3";
     C.extern "ocsigen.xhtml" ~cma:"xhtml";

     flag ["ocaml"; "doc"; "use_extLib"] (S[A"-I"; A (C.lib "extlib")]);

     let flag_bn l x =
       flag ("byte"::l) x;
       flag ("native"::l) x
     in

     flag_bn ["link"; "ocaml"; "use_netstring"; "thread"] (S[A"netstring_mt.cmo"]);
     flag_bn ["link"; "ocaml"; "use_equeue"; "thread"] (S[A"unixqueue_mt.cmo"]);

| _ -> ()
end
